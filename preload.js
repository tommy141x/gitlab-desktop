/**
 * The preload script runs before. It has access to web APIs
 * as well as Electron's renderer process modules and some
 * polyfilled Node.js functions.
 *
 * https://www.electronjs.org/docs/latest/tutorial/sandbox
 */

const { contextBridge, ipcRenderer } = require('electron');
const { CustomTitlebar, TitlebarColor } = require('custom-electron-titlebar')
const path = require('path')

window.addEventListener('DOMContentLoaded', () => {
    // Style the titlebar
    new CustomTitlebar({
        backgroundColor: TitlebarColor.fromHex('#222222'),
        itemBackgroundColor: TitlebarColor.fromHex('#333333'),
        menuSeparatorColor: TitlebarColor.fromHex('#111111'),
        menuTransparency: 0.2,
        transparent: 0.5,
        onlyShowMenubar: true,
        enableMnemonics: true,
        icon: path.join(__dirname, 'icon.png'),
    })
});

// Expose ipc methods that allow the renderer process to use Electron APIs
contextBridge.exposeInMainWorld(
    'electron',
    {
        ipcRenderer: {
            send: (channel, data) => ipcRenderer.send(channel, data),
            on: (channel, listener) => {
                // Deliberately strip event as it includes `sender`
                ipcRenderer.on(channel, (event, ...args) => listener(...args));
            },
            removeAllListeners: (channel) => ipcRenderer.removeAllListeners(channel)
        }
    }
);

