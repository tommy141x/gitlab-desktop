// Modules to control application life and create native browser window
const {app, BrowserWindow, Menu, nativeImage, BrowserView, ipcMain, nativeTheme} = require('electron')
const {setupTitlebar, attachTitlebarToWindow} = require('custom-electron-titlebar/main')
const path = require('path')
let currentZoomFactor = 1.0;
let appWindow;

// Setup the titlebar
setupTitlebar()

function createWindow(){
    // Create the app browser window.
    appWindow = new BrowserWindow({
        width: 1000,
        height: 600,
        frame: false,
        titleBarStyle: 'hidden',
        titleBarOverlay: true,
        webPreferences: {
            contextIsolation: true, // This is my first ever electron app using this option
            nodeIntegration: false, // This is my first ever electron app using this option
            sandbox: false,
            preload: path.join(__dirname, 'preload.js')
        }
    })
    
   /* const view = new BrowserView()
  appWindow.setBrowserView(view)
  view.setBounds({ x: 0, y: 0, width: 300, height: 300 })
  appWindow.on('resize', () => {
    let [width, height] = appWindow.getSize();
    let viewWidth = Math.floor(width * 0.8);
    let viewHeight = Math.floor(height * 0.8);
    let x = Math.floor(width * 0.1);
    let y = Math.floor(height * 0.1);
    view.setBounds({x: x, y: y, width: viewWidth, height: viewHeight});
 });

 // Set the initial bounds of the BrowserView
 appWindow.emit('resize');
  //view.webContents.loadURL('https://gitlab.com')
*/
    // Set the menu
    const menu = Menu.buildFromTemplate(menuTemplate)
    Menu.setApplicationMenu(menu);

    // and load the index.html of the app.
    appWindow.loadFile('index.html').then(() => {
        appWindow.webContents.send('menu-template', JSON.stringify(menuTemplate));
    });

    // Open the DevTools. (Only for development)
    appWindow.webContents.openDevTools()

    // Attach the titlebar to the window
    attachTitlebarToWindow(appWindow)
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
app.whenReady().then(() => {
    createWindow()

    app.on('activate', function () {
        // On macOS it's common to re-create a window in the app when the
        // dock icon is clicked and there are no other windows open.
        if (BrowserWindow.getAllWindows().length === 0) createWindow()
    })
})

// Quit when all windows are closed, except on macOS. There, it's common
// for applications and their menu bar to stay active until the user quits
// explicitly with Cmd + Q.
app.on('window-all-closed', function () {
    if (process.platform !== 'darwin') app.quit()
})

// Update the menu from the renderer process
ipcMain.handle('updateMenu', (menu) => {
    Menu.setApplicationMenu(menu);
})

// Menu template
const menuTemplate = [
    {
        label: 'File',
        submenu: [
            {
                label: 'New repository',
                click: () => {
                    appWindow.webContents.send('menu-func', 'new-repository');
                },
                accelerator: 'Ctrl+N'
            },
            {type: 'separator'},
            {
                label: 'Add local repository',
                click: () => {
                    appWindow.webContents.send('menu-func', 'add-local-repository');
                },
                accelerator: 'Ctrl+O'
            },
            {
                label: 'Clone repository',
                click: () => {
                    appWindow.webContents.send('menu-func', 'clone-repository');
                },
                accelerator: 'Ctrl+Shift+O'
            },
            {type: 'separator'},
            {
                label: 'Options',
                click: () => {
                    appWindow.webContents.send('menu-func', 'options');
                },
                accelerator: 'Ctrl+Comma'
            },
            {type: 'separator'},
            {
                label: 'Exit',
                click: () => {
                    app.quit();
                },
                accelerator: 'Alt+F4'
            },
        ]
    },
    {
        label: 'Edit',
        submenu: [
            {
                label: 'Undo',
                click: () => {
                    appWindow.webContents.send('menu-func', 'undo');
                },
                enabled: false,
                accelerator: 'Ctrl+Z'
            },
            {
                label: 'Redo',
                click: () => {
                    appWindow.webContents.send('menu-func', 'redo');
                },
                accelerator: 'Ctrl+Shift+Z'
            },
            {type: 'separator'},
            {
                label: 'Cut',
                click: () => {
                    appWindow.webContents.send('menu-func', 'cut');
                },
                accelerator: 'Ctrl+X'
            },
            {
                label: 'Copy',
                click: () => {
                    appWindow.webContents.send('menu-func', 'copy');
                },
                accelerator: 'Ctrl+C'
            },
            {
                label: 'Paste',
                click: () => {
                    appWindow.webContents.send('menu-func', 'paste');
                },
                accelerator: 'Ctrl+V'
            },
            {
                label: 'Select all',
                click: () => {
                    appWindow.webContents.send('menu-func', 'select-all');
                },
                accelerator: 'Ctrl+A'
            },
            {type: 'separator'},
            {
                label: 'Find',
                click: () => {
                    appWindow.webContents.send('menu-func', 'find');
                },
                accelerator: 'Ctrl+F'
            }
        ]
    },
    {
        label: 'View',
        submenu: [
            {
                label: 'Changes',
                click: () => {
                    appWindow.webContents.send('menu-func', 'show-changes');
                },
                accelerator: 'Ctrl+1'
            },
            {
                label: 'History',
                click: () => {
                    appWindow.webContents.send('menu-func', 'show-history');
                },
                accelerator: 'Ctrl+2'
            },
            {
                label: 'Repository list',
                click: () => {
                    appWindow.webContents.send('menu-func', 'show-repository-list');
                },
                accelerator: 'Ctrl+T'
            },
            {
                label: 'Branches list',
                click: () => {
                    appWindow.webContents.send('menu-func', 'show-branches-list');
                },
                accelerator: 'Ctrl+B'
            },
            {type: 'separator'},
            {
                label: 'Go to Summary',
                click: () => {
                    appWindow.webContents.send('menu-func', 'go-to-summary');
                },
                accelerator: 'Ctrl+G'
            },
            {
                label: 'Show stashed changes',
                click: () => {
                    appWindow.webContents.send('menu-func', 'show-stashed-changes');
                },
                accelerator: 'Ctrl+H'
            },
            {
                label: 'Toggle full screen',
                click: () => {

                },
                accelerator: 'F11'
            },
            {type: 'separator'},
            {
                label: 'Reset zoom',
                click: () => {

                },
                accelerator: 'Ctrl+0'
            },
            {
                label: 'Zoom in',
                click: () => {
                    currentZoomFactor += 0.1; // Increase by 10%
                    appWindow.webContents.setZoomFactor(currentZoomFactor);
                },
                accelerator: 'Ctrl+Plus'
            },
            {
                label: 'Zoom out',
                click: () => {
                    currentZoomFactor -= 0.1; // Decrease by 10%
                    appWindow.webContents.setZoomFactor(currentZoomFactor);
                },
                accelerator: 'Ctrl+Minus'
            },
            {type: 'separator'},
            {
                label: 'Toggle developer tools',
                click: () => {
                    appWindow.webContents.toggleDevTools();
                },
                accelerator: 'Ctrl+Shift+I'
            },
        ]
    },
    {
        label: 'Repository',
        submenu: [
            {
                label: 'Push',
                click: () => {
                    appWindow.webContents.send('menu-func', 'push');
                },
                accelerator: 'Ctrl+P'
            },
            {
                label: 'Pull',
                click: () => {
                    appWindow.webContents.send('menu-func', 'pull');
                },
                accelerator: 'Ctrl+Shift+P'
            },
            {
                label: 'Fetch',
                click: () => {
                    appWindow.webContents.send('menu-func', 'fetch');
                },
                accelerator: 'Ctrl+Shift+T'
            },
            {
                label: 'Remove',
                click: () => {
                    appWindow.webContents.send('menu-func', 'remove-repository');
                },
                accelerator: 'Ctrl+Backspace'
            },
            {type: 'separator'},
            {
                label: 'View on GitLab',
                click: () => {
                    appWindow.webContents.send('menu-func', 'view-on-gitlab');
                },
                accelerator: 'Ctrl+Shift+G'
            },
            {
                label: 'Open in Terminal',
                click: () => {
                    appWindow.webContents.send('menu-func', 'open-in-terminal');
                },
                accelerator: 'Ctrl+`'
            },
            {
                label: 'Show in your File Manager',
                click: () => {
                    appWindow.webContents.send('menu-func', 'show-in-file-manager');
                },
                accelerator: 'Ctrl+Shift+F'
            },
            {
                label: 'Open in Visual Studio Code (or other)',
                click: () => {
                    appWindow.webContents.send('menu-func', 'open-in-editor');
                },
                accelerator: 'Ctrl+Shift+A'
            },
            {type: 'separator'},
            {
                label: 'Create issue on GitLab',
                click: () => {
                    appWindow.webContents.send('menu-func', 'create-issue');
                },
                accelerator: 'Ctrl+I'
            },
            {type: 'separator'},
            {
                label: 'Repository Settings',
                click: () => {
                    appWindow.webContents.send('menu-func', 'repository-settings');
                },
            },
        ]
    },
    {
        label: 'Branch',
        submenu: [
            {
                label: 'New branch',
                click: () => {
                    appWindow.webContents.send('menu-func', 'new-branch');
                },
                accelerator: 'Ctrl+Shift+N'
            },
            {
                label: 'Rename',
                click: () => {
                    appWindow.webContents.send('menu-func', 'rename-branch');
                },
                accelerator: 'Ctrl+Shift+R'
            },
            {
                label: 'Delete',
                click: () => {
                    appWindow.webContents.send('menu-func', 'delete-branch');
                },
                accelerator: 'Ctrl+Shift+D'
            },
            {type: 'separator'},
            {
                label: 'Discard all changes',
                click: () => {
                    appWindow.webContents.send('menu-func', 'discard-all-changes');
                },
                accelerator: 'Ctrl+Shift+Backspace'
            },
            {
                label: 'Stash all changes',
                click: () => {
                    appWindow.webContents.send('menu-func', 'stash-all-changes');
                },
                accelerator: 'Ctrl+Shift+S'
            },
            {type: 'separator'},
            {
                label: 'Update from main',
                click: () => {
                    appWindow.webContents.send('menu-func', 'update-from-main');
                },
                accelerator: 'Ctrl+Shift+U'
            },
            {
                label: 'Compare to branch',
                click: () => {
                    appWindow.webContents.send('menu-func', 'compare-to-branch');
                },
                accelerator: 'Ctrl+Shift+B'
            },
            {
                label: 'Merge into current branch',
                click: () => {
                    appWindow.webContents.send('menu-func', 'merge-to-current-branch');
                },
                accelerator: 'Ctrl+Shift+M'
            },
            {
                label: 'Squash and merge into current branch',
                click: () => {
                    appWindow.webContents.send('menu-func', 'squash-merge');
                },
                accelerator: 'Ctrl+Shift+H'
            },
            {
                label: 'Rebase current branch',
                click: () => {
                    appWindow.webContents.send('menu-func', 'rebase');
                },
                accelerator: 'Ctrl+Shift+E'
            },
            {type: 'separator'},
            {
                label: 'Compare on GitLab',
                click: () => {
                    appWindow.webContents.send('menu-func', 'compare-on-gitlab');
                },
                accelerator: 'Ctrl+Shift+C'
            },
            {
                label: 'View branch on GitLab',
                click: () => {
                    appWindow.webContents.send('menu-func', 'view-branch-on-gitlab');
                },
                accelerator: 'Alt+Ctrl+B'
            },
            {
                label: 'Preview pull request',
                click: () => {
                    appWindow.webContents.send('menu-func', 'preview-pull-request');
                },
                accelerator: 'Alt+Ctrl+P'
            },
            {
                label: 'Create pull request',
                click: () => {
                    appWindow.webContents.send('menu-func', 'pull-request');
                },
                accelerator: 'Ctrl+R'
            },
        ]
    },
    {
        label: 'Help',
        submenu: [
            {
                label: 'Report issue',
                click: () => {
                    appWindow.webContents.send('menu-func', 'report-issue');
                },
            },
            {
                label: 'Show keyboard shortcuts',
                click: () => {
                    appWindow.webContents.send('menu-func', 'shortcuts');
                },
            },
            {
                label: 'Show logs in your File Manager',
                click: () => {

                },
            },
            {type: 'separator'},
            {
                label: 'About GitLab Desktop',
                click: () => {
                    appWindow.webContents.send('menu-func', 'about');
                },
            },
        ]
    },
]
