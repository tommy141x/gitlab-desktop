
let menuTemplate;

window.electron.ipcRenderer.on('menu-template', (template) => {
    menuTemplate = JSON.parse(template);
});

// All function calls from menu items are handled here
window.electron.ipcRenderer.on('menu-func', (func) => {
    switch (func) {
        case 'new-repository':
            // TODO: Implement
            break;
        case 'add-local-repository':
            // TODO: Implement
            break;
        case 'clone-repository':
            // TODO: Implement
            break;
        case 'options':
            // TODO: Implement
            break;
        case 'undo':
            // TODO: Implement
            break;
        case 'redo':
            // TODO: Implement
            break;
        case 'cut':
            // TODO: Implement
            break;
        case 'copy':
            // TODO: Implement
            break;
        case 'paste':
            // TODO: Implement
            break;
        case 'select-all':
            // TODO: Implement
            break;
        case 'find':
            // TODO: Implement
            break;
        default:
            break;
    }
});

